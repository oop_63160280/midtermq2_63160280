/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.karntima.midtermq2;

/**
 *
 * @author User
 */
public class TestWashing {

    public static void main(String[] args) {
        WashingMachine washingmachine = new WashingMachine(14, 60, "H");
        washingmachine.Washing();

        System.out.println("--------------------");
        washingmachine.setWCT(9, 40, "W");
//        washingmachine.setWeight(9);
//        washingmachine.setCoin(40);
//        washingmachine.setTemp("W");
        washingmachine.Washing();

        System.out.println("--------------------");
        washingmachine.Washing();

        System.out.println("--------------------");
        washingmachine.Washing(9);

        System.out.println("--------------------");
        washingmachine.Washing(25, "C");

        System.out.println("--------------------");
        SmallWashingMachine small = new SmallWashingMachine(40, "H");
        small.welcomWashing();
        small.Washing();

        System.out.println("--------------------");
        MediumWashingMachine medium = new MediumWashingMachine(60, "C");
        medium.welcomWashing();
        medium.Washing();

        System.out.println("--------------------");
        LargeWashingMachine large = new LargeWashingMachine(100, "W");
        large.welcomWashing();
        large.Washing();

        System.out.println("--------------------");
        washingmachine.welcomWashing();
        washingmachine.Washing(25, "C");

    }
}
